# learning-materials

Links to useful learning materials for Python and Data Science

## Python tutorials for newbies
* [Practical Python](https://github.com/dabeaz-course/practical-python/blob/master/Notes/Contents.md): Fantastic tutorial on Python language features suitable for beginners who have coding aptitude or some coding experience.
* [Hacker Earth](https://www.hackerearth.com/de/practice/python/getting-started/input-and-output/tutorial/): Even though this is a recruitment/assessment platform, their Python tutorials are great. 